#!/bin/bash

readonly role=$1
readonly current_branch=`git branch | grep '\*' | awk '{print $2}'`

echo "role: $role"
echo "current_branch: $current_branch"

# echo "Ctrl-C to cancel"
# sleep 3

git rm -r --cached --quiet $role
git commit -m "Move $role to a feature branch"
git checkout -b $role
git add $role
git commit -m "[WIP] $role role"
git push --set-upstream origin $role
git checkout $current_branch
