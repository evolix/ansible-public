# Ansible-public

This repository contains some stuff related to Ansible (except roles, that are in our [`ansible-roles` repository](https://forge.evolix.org/projects/ansible-roles/repository).

The main part is our Evolinux playbook, that we use to customize a Linux Debian server.
It also contains our conventions for Ansible.

For beginners, you can read the topic [https://wiki.evolix.org/HowtoAnsible](HowtoAnsible).

## Evolinux

To set up a server according to "The Evolix Way", you have to install the current Debian stable version.

### Prerequisites

We recommend using Debian with the "netinst" CD image, here are some key settings :

* Language : English
* Location : Other -> Europe -> France
* Locales : en_US.UTF-8 UTF-8
* Keyboard : French

We recommend to install only the minimum packages. At the "tasksel" step, choose only the **SSH server** item.

### `evolinux.yml` playbook

When the server is installed and rebooted, with a functioning SSH access, make sure that those packages are installed, for Ansible to be able to run.

```
# apt install sudo dbus python-apt
```

You'll need a privileged user (preferably not root, but a regular user with full sudo permissions).
You can customize the playbook or add command line options to specify with user to use for the ssh connection and which privilege escalation to use (we like to use "sudo").

With some roles, you'll have to populate the `vars/main.yml` file with values required by the "admin-users" and "evolinux-base" roles. You should refer to their respective documentation.

You'll also need to configure Ansible to include the location of the "ansible-roles" path in the `roles_path` value. We recommend using `$HOME/.ansible.cfg` with this :

```
[defaults]
roles_path = /path/to/ansible-roles
```

You may want to configure the inventory to your needs. We usually put it in `inventory/hosts` along the conventionnal files for host and group vars.

With all that, you can run the evolinux.yml playbook and have your Debian customized :

```
$ ansible-playbook playbooks/evolinux.yml -i inventory/hosts -K
```

## Conventions

Our conventions for writting Ansible roles, playbooks… are in the CONVENTIONS.md file.

## Tests

It's possible to use the `test/vagrant.yml` playbook locally, to test and debug the roles on a virtual machine.

It works with a Virtualbox VM, driven by Vagrant.

To install Virtualbox and Vagrant (version 1.8 is not available on Debian repositories yet) :

```
# apt install virtualbox
# curl -O https://releases.hashicorp.com/vagrant/1.8.5/vagrant_1.8.5_x86_64.deb /tmp/
# dpkg -i /tmp /vagrant_1.8.5_x86_64.deb
```

To bring the VM up and run the playbook, simply run `$ vagrant up` from the root of this repository.

To destroy the VM and start again from scratch : `$ vagrant destroy && vagrant up`.
